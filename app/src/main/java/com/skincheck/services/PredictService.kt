package com.skincheck.services

import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStream
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class PredictService {

    private val ip = "35.172.214.234"

    fun getImagePrediction(imageData: String): String {
        return post(imageData, "getPrediction")
    }

    private fun post(jsonData: String, suffix: String): String {
        val url = URL("http:/$ip:80/$suffix")
        val conn = url.openConnection() as HttpURLConnection
        conn.doOutput = true
        conn.requestMethod = "POST"
        conn.setRequestProperty("Content-Type", "application/json")
        conn.setRequestProperty("Accept", "application/json")
        val dataOutputStream: OutputStream? = conn.outputStream
        dataOutputStream?.write(jsonData.toByteArray(Charsets.UTF_8))
        conn.connect()
        if (conn.responseCode != 200) {
            throw Exception("Failed to post segmentsData to: " + url + ", HTTP error code : " + conn.responseCode)
        }

        val br = BufferedReader(InputStreamReader(conn.inputStream))
        var data = ""
        var line: String? = ""
        while (line != null) {
            line = br.readLine()
            if (line != null) {
                data = data + line + "\n"
            }
        }
        br.close()
        conn.disconnect()
        return data

    }
}