package com.skincheck.viewmodels

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.skincheck.R
import com.skincheck.model.SinglePrediction

class ResultsAdapter(private val context: Context, private val dataset: List<SinglePrediction>) : RecyclerView.Adapter<ResultsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context).inflate(
            R.layout.prediction_result,
            parent,
            false
        ) as ConstraintLayout

        return ViewHolder(constraintLayout)
    }

    override fun getItemCount(): Int = dataset.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nameOfDiseaseTV.text = dataset[position].nameOfDisease
        holder.percentageTV.text = String.format("%.3f %%", dataset[position].percentage)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameOfDiseaseTV = itemView.findViewById<TextView>(R.id.name_of_disease_text)!!
        val percentageTV = itemView.findViewById<TextView>(R.id.percentage_text)!!
    }
}