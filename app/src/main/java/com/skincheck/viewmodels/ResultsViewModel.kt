package com.skincheck.viewmodels

import androidx.lifecycle.ViewModel
import com.google.gson.GsonBuilder
import com.skincheck.model.PredictionResults

class ResultsViewModel : ViewModel() {
    private val gson = GsonBuilder().create()

    fun parseResults(resultsText : String): PredictionResults? {
        return gson.fromJson(resultsText, PredictionResults::class.java)
    }
}