package com.skincheck.viewmodels

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import com.google.gson.GsonBuilder
import com.skincheck.services.PredictService
import java.io.ByteArrayOutputStream

class ChooseImageViewModel : ViewModel(){
    private val gson = GsonBuilder().setPrettyPrinting().create()
    private val predictService: PredictService = PredictService()

    fun getPredictions(bitmap: Bitmap): String {
        val byteStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteStream)
        val imageBytes = byteStream.toByteArray()
        val decode = android.util.Base64.encodeToString(imageBytes, android.util.Base64.DEFAULT)
        return predictService.getImagePrediction(gson.toJson(decode))
    }
}