package com.skincheck.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.skincheck.R
import com.skincheck.model.PredictionResults
import com.skincheck.model.SinglePrediction
import com.skincheck.viewmodels.ResultsAdapter
import com.skincheck.viewmodels.ResultsViewModel
import kotlinx.android.synthetic.main.fragment_results.*
import androidx.recyclerview.widget.DividerItemDecoration




class ResultsFragment : Fragment() {
    private lateinit var resultsViewModel: ResultsViewModel
    private lateinit var results : PredictionResults

    private val resultsText by lazy {
        ResultsFragmentArgs.fromBundle(arguments!!).results
    }

    private val chosenImage by lazy {
        ResultsFragmentArgs.fromBundle(arguments!!).image
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        activity?.run {
            resultsViewModel = ViewModelProvider(this).get(ResultsViewModel::class.java)
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_results, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        results = resultsViewModel.parseResults(resultsText)!!

        predicted_image.setImageBitmap(chosenImage)
        results.sort()

        setAdapterForRecyclerView()
        setDividerForRecyclerView()
    }

    private fun setAdapterForRecyclerView() {
        results_list.layoutManager = LinearLayoutManager(context)
        val adapter = ResultsAdapter(context!!, results.results)
        results_list.adapter = adapter

    }

    private fun setDividerForRecyclerView() {
        val dividerItemDecoration = DividerItemDecoration(
            results_list.context,
            (results_list.layoutManager as LinearLayoutManager).orientation
        )
        results_list.addItemDecoration(dividerItemDecoration)
    }


}
