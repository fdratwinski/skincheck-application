package com.skincheck.views

import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import com.skincheck.R
import kotlinx.android.synthetic.main.fragment_choose_image.*
import android.provider.MediaStore
import android.app.Activity
import android.graphics.Bitmap
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.skincheck.viewmodels.ChooseImageViewModel
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import android.net.Uri
import androidx.core.content.FileProvider
import androidx.navigation.fragment.findNavController
import android.os.AsyncTask


class ChooseImageFragment : Fragment() {
    private lateinit var chooseImageViewModel: ChooseImageViewModel
    private val PICK_IMAGE_REQUEST_CODE = 10
    private val MAKE_IMAGE_REQUEST_CODE = 11
    private var currentImage: Bitmap? = null
    var currentPhotoPath: String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        activity?.run {
            chooseImageViewModel = ViewModelProvider(this).get(ChooseImageViewModel::class.java)
        }

        if (Build.VERSION.SDK_INT > 22) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 1
            )
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_image, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setListenerForCameraButton()
        setListenerForGalleryButton()
        setListenerForPredictionButton()
    }

    private fun setListenerForPredictionButton() {
        predict_button.setOnClickListener {
            ImageAsyncTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        }
    }

    private fun setListenerForGalleryButton() {
        choose_image_button.setOnClickListener {
            val photoPickerIntent = Intent(Intent.ACTION_GET_CONTENT)
            photoPickerIntent.type = "image/*"
            startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), PICK_IMAGE_REQUEST_CODE)
        }
    }

    private fun setListenerForCameraButton() {
        make_image_button.setOnClickListener {
            dispatchTakePictureIntent()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            var bitmap: Bitmap? = null
            if (requestCode == PICK_IMAGE_REQUEST_CODE) {
                val selectedImage = data!!.data
                bitmap = MediaStore.Images.Media.getBitmap(context!!.contentResolver, selectedImage)
            } else if (requestCode == MAKE_IMAGE_REQUEST_CODE) {
                val file = File(currentPhotoPath)
                bitmap = MediaStore.Images.Media
                    .getBitmap(context!!.contentResolver, Uri.fromFile(file))
            }
            if (bitmap!!.width < 320 || bitmap!!.height < 320) {
                error_text.visibility = View.VISIBLE
                predict_button.visibility = View.INVISIBLE
                error_text.text = "Resolution of chosen image is too low (minimum 320x320)"
            } else {
                error_text.visibility = View.INVISIBLE
                currentImage = bitmap
                predict_button.visibility = View.VISIBLE
            }
            chosen_image.setImageBitmap(bitmap)
            image_placeholder.visibility = View.INVISIBLE
        }
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )
        currentPhotoPath = image.absolutePath
        return image
    }


    @Throws(IOException::class)
    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(context!!.packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
            }

            if (photoFile != null) {
                val photoUri = FileProvider.getUriForFile(
                    context!!,
                    "com.skincheck.fileprovider",
                    photoFile
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(takePictureIntent, MAKE_IMAGE_REQUEST_CODE)
            }
        }
    }

    private class ImageAsyncTask
    internal constructor(val context: ChooseImageFragment) : AsyncTask<Int, Int, String?>() {
        override fun doInBackground(vararg params: Int?): String? {
            return context.chooseImageViewModel.getPredictions(context.currentImage!!)
        }

        override fun onPreExecute() {
            context.loading_icon.visibility = View.VISIBLE
            context.transparent_background.visibility = View.VISIBLE
            context.choose_image_button.isEnabled = false
            context.make_image_button.isEnabled = false
            context.predict_button.isEnabled = false
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (result.equals("Not an image of a skin\n")) {
                context.error_text.visibility = View.VISIBLE
                context.predict_button.visibility = View.INVISIBLE
                context.error_text.text = "Chosen image is not correct"
                context.loading_icon.visibility = View.INVISIBLE
                context.transparent_background.visibility = View.INVISIBLE
                context.choose_image_button.isEnabled = true
                context.make_image_button.isEnabled = true
                context.predict_button.isEnabled = true

            } else {
                val actionChooseImageToResults =
                    ChooseImageFragmentDirections.actionChooseImageToResults(result!!, context.currentImage!!)
                context.findNavController().navigate(actionChooseImageToResults)
            }
        }
    }


}
