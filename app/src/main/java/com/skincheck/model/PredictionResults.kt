package com.skincheck.model

import com.google.gson.annotations.SerializedName

class PredictionResults(@SerializedName("predictions") var results: List<SinglePrediction>) {

    fun sort() {
        results = results.sortedByDescending { singlePrediction -> singlePrediction.percentage }
    }
}