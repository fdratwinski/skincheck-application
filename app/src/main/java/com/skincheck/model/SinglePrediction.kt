package com.skincheck.model

import com.google.gson.annotations.SerializedName

class SinglePrediction(
    @SerializedName("name_of_disease") val nameOfDisease: String,
    @SerializedName("percentage") val percentage: Float
)